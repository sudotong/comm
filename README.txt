Mission 7 rules:
http://www.aerialroboticscompetition.org/downloads/mission7rules_081514.pdf

Our Pixhawk code repo:
https://bitbucket.org/mrkris13/pixhawk
(vs official at https://github.com/PX4/Firmware)

Guide to running your own sample app on the pixhawk:
https://pixhawk.org/dev/px4_simple_app

uORB publishing and subscribing:
https://pixhawk.org/dev/shared_object_communication

handling an incoming mavlink message:
https://pixhawk.org/dev/mavlink#other_functions

NSH via serial connection:
https://pixhawk.org/users/serial_connection

Flashing and building our pixhawk code:
https://pixhawk.org/dev/nuttx/building_and_flashing_console










Interesting links for mavros (but we're not going this route ATM)
http://www.pixhawk.org/dev/ros/start
https://groups.google.com/forum/#!topic/px4users/IC1iz_TsL_Y
