/** @file
 *	@brief MAVLink comm protocol built from mitmessage.xml
 *	@see http://mavlink.org
 */
#ifndef MAVLINK_VERSION_H
#define MAVLINK_VERSION_H

#define MAVLINK_BUILD_DATE "Fri Jan 30 20:26:26 2015"
#define MAVLINK_WIRE_PROTOCOL_VERSION "1.0"
#define MAVLINK_MAX_DIALECT_PAYLOAD_SIZE 21
 
#endif // MAVLINK_VERSION_H
